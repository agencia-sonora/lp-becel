const path = require('path');
const settings = require('./settings');
const webpack = require('webpack');

module.exports = {
  mode: 'none',
  entry: {
    App: settings.themeLocation + "resources/js/scripts.js",
    Vendor: settings.themeLocation + "resources/js/vendors/vendors.js",
  },
  output: {
    path: path.resolve(__dirname, settings.themeLocation + "resources/js/temp"),
    filename: "[name].js",
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  resolve: {
    alias: {
      "ScrollMagic": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js'),
      "debug.addIndicators": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js')
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    new webpack.SourceMapDevToolPlugin({})
  ],
  devtool: false,
  
}