const {src, dest, watch, series, parallel} = require('gulp');
const webpack = require('webpack');
const sass = require('gulp-sass');
const sassPaths = ['./node_modules'];
const plumber = require('gulp-plumber');
const postcssImport = require('postcss-import');
const postcss = require('gulp-postcss');
const cssnano = require('cssnano');
const cssautoprefixer = require('autoprefixer');
const concat = require('gulp-concat');
const terser = require('gulp-terser');
const strip = require('gulp-strip-comments');
const sourcemaps = require('gulp-sourcemaps');
const browsersync = require('browser-sync').create();

//  Settings
const settings = require('./settings');
const { stream } = require('browser-sync');
const browserSync = require('browser-sync');

// Sour Paths
const jsSourcePath = 'resources/js/**/*.js';
const cssSourcePath = 'resources/sass/**/*.scss';

// Paths
const jsPath = 'resources/js/temp/App.js';
const jsVendorPath = 'resources/js/temp/Vendor.js';
const cssPath = 'resources/sass/styles.scss';

// Sass Task
function scssTask(){
    return src(settings.themeLocation + cssPath)
    // .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sass({includePaths: sassPaths}))
    .pipe(postcss([postcssImport(), cssautoprefixer(), cssnano()]))
    // .pipe(sourcemaps.write('.'))
    .pipe(dest(settings.themeLocation + '.'))
    .pipe(browsersync.stream())
}

// Javascript Task
function scripts(callback){
    webpack(require('./webpack.config.js'), (err, stats) => {
        if(err){
            console.log(err.toString());
        }
        console.log(stats.toString());
        callback();
    });
}
function jsTask(){
    return src(settings.themeLocation + jsPath)
    // .pipe(sourcemaps.init())
    .pipe(concat('scripts.js'))
    .pipe(terser())
    .pipe(strip())
    // .pipe(sourcemaps.write('.'))
    .pipe(dest(settings.themeLocation + '.'))
}
function jsVendor(){
    return src(settings.themeLocation + jsVendorPath)
    // .pipe(sourcemaps.init())
    .pipe(concat('vendors.js'))
    .pipe(terser())
    .pipe(strip())
    // .pipe(sourcemaps.write('.'))
    .pipe(dest(settings.themeLocation + '.'))
}

// Browsersync Task
function BrowserSyncServer(cb){
    browsersync.init({
        notify: false,
        proxy: settings.urlToPreview
    })
    cb();
}

function browsersyncReload(cb){
    browsersync.reload();
    cb();
}

function browsersyncLive(cb){
    browsersync.stream();
    cb();
}

// Watch Task
function watchTask(){
  watch('app/**/*.php', browsersyncReload)
  watch([settings.themeLocation + cssSourcePath], series(scssTask, browsersyncLive))
  watch([settings.themeLocation + jsSourcePath], series(scripts, jsTask, jsVendor, browsersyncReload))
}

// Default Gulp Task
exports.default = series(
    parallel(
    scssTask
    ),
    jsTask,
    BrowserSyncServer,
    watchTask
    )

