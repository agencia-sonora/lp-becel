<?php include('./resources/views/parts/header.php');?>

<section class="ganhadores section">
  <div class="container">
    <div class="row">
      <h1>GANHADORES</h1>
      <h2>VEJA QUEM GANHOU OS PRÊMIOS</h2>
    </div>
    <div class="row">
      <p>&nbsp;</p>
    </div>
  </div>
  <div class="container-row backgroundRadial">
    <div class="row">
      <div class="ganhadores__mapa">
        <h3>SELECIONE NO MAPA O ESTADO PARA SABER QUANTAS PESSOAS
          JÁ GANHARAM OS PRÊMIOS INSTANTÂNEOS.
        </h3>

        <div id="vmap" style="width: 100%;">
          <div id="region" class="square" style="display: none;"></div>
        </div>
        <div class="clearFix"></div>
        <!-- Estados -->
        <div id="AC" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO 2</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="AM" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="RO" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="MT" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="RR" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="PA" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="AP" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="MA" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="PI" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="TO" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="MS" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="GO" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="DF" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="BA" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="PE" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="CN" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="PB" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="AL" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="CE" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="SE" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="MG" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="ES" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="RJ" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="SP" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="PR" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="SC" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="RS" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <div id="RN" style="display: none;">
          <div class="ganhadores__box">
            <div class="ganhadores__box__estado">
              <h4>NOME DO ESTADO</h4>
              <small>1.580 PREMIADOS</small>
            </div>
            <div class="ganhadores__box__atualizacao">
              <p>ÚLTIMA ATUALIZAÇÃO: 05/02/21</p>
            </div>
          </div>
        </div>
        <!-- /Estados -->

        <h4>VEJA ABAIXO OS GANHADORES DOS PRÊMIOS SEMANAIS.</h4>
        <div class="ganhadores__mapa__semanais">
          <table width="100%" id="ganhadoresSemanais" border="0" cellspacing="0" cellpadding="0">
            <tbody>
              <tr>
                <th width="50%">NOME DO GANHADOR (A)</th>
                <th width="25%">CIDADE</th>
                <th width="25%">ESTADO</th>
              </tr>
              <tr>
                <td>MARIANA PACHECO</td>
                <td>SÃO PAULO</td>
                <td>SP</td>
              </tr>
              <tr>
                <td>MURILLO DIAS</td>
                <td>CAMPINAS</td>
                <td>SP</td>
              </tr>
              <tr>
                <td>MARIA REGINA SANTOS</td>
                <td>SALVADOR</td>
                <td>BA</td>
              </tr>
              <tr>
                <td>DIEGO BARBOSA</td>
                <td>RIO DE JANEIRO</td>
                <td>RJ</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</section>

<!-- Estados Vencedores -->
<script>
function escapeXml(string) {
  return string.replace(/[<>]/g, function(c) {
    switch (c) {
      case '<':
        return '\u003c';
      case '>':
        return '\u003e';
    }
  });
}
var pinsClass = '<div class="map-pin"><span></span></div>'
var pins = {
  // 'ac', 'am', 'ro', 'mt', 'rr', 'pa', 'ap', 'ma', 'pi', 'to', 'ms', 'go', 'df', 'ba', 'pe', 'cn', 'pb', 'al', 'ce', 'se', 'mg', 'es', 'rj', 'sp', 'pr', 'sc', 'rs', 'rn'
  am: escapeXml(pinsClass),
  pa: escapeXml(pinsClass),
  mt: escapeXml(pinsClass),
  ba: escapeXml(pinsClass),
  sp: escapeXml(pinsClass)
}
</script>

<?php include('./resources/views/parts/footer.php');?>