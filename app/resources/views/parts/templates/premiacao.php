<section id="premiacao" class="section headlinesSections premiacao">
  <div class="container-fluid">
    <div class="row">
      <h1>PREMIAÇÃO</h1>
    </div>
    <div class="row">
      <div class="premiacao__foto" data-aos="fade-up">
        <picture>
          <source media="(max-width: 540px)" srcset="resources/media/images/premiacao_img_mobile.png">
          <img src="resources/media/images/premiacao_img_desk.png"
            alt="Concorra TODA semana!*  Bicicletas elétricas e iphone 12">
        </picture>
      </div>
    </div>
    <div class="row">
      <div class="premiacao__mais" data-aos="fade-up">
        <picture>
          <source media="(max-width: 540px)" srcset="resources/media/images/premiacao__mais-mobile.png">
          <img src="resources/media/images/premiacao__mais-desk.png"
            alt="E ainda... mais de 1.000 prêmios de até R$500,00** na hora">
        </picture>
      </div>
    </div>
    <div class="row">
      <a href="#" class="btn btn--green btn--center btn--modal">CADASTRE SUAS COMPRAS</a>
    </div>
  </div>
</section>