<!-- Modal -->
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <h1>
            ACESSE AGORA E CONCORRA A <br>
            PRÊMIOS INCRÍVEIS.
          </h1>
          <div class="row">
            <form method="post" action="" class="col-12 login__form">
              <h2>JÁ TEM CADASTRO?</h2>
              <div class="d-sm-flex">
                <div class="form-group col-sm-6">
                  <label for="login-cpf">CPF</label>
                  <input id="login-cpf" class="form-control form-control-lg" type="text" name="text" maxlength="14">
                </div>
                <div class="form-group col-sm-6">
                  <label for="login-senha">Senha</label>
                  <input id="login-senha" class="form-control form-control-lg" type="password" name="password">
                </div>
              </div>
              <button type="submit" class="btn">ENTRAR</button>
              <div class="clear"></div>
              <a href="#" class="login__form__esqueciasenha">ESQUECI MINHA SENHA</a>
            </form>
          </div>
          <div class="row">
            <form style="display: none;" method="get" action="" class="col-12 login__form--recuperasenha">
              <h2>RECUPERE SUA SENHA</h2>
              <div class="d-sm-flex">
                <div class="form-group col-sm-12">
                  <label for="recupere-cpf">CPF</label>
                  <input id="recupere-cpf" class="form-control form-control-lg" type="text" name="text" maxlength="14">
                </div>
              </div>
              <button type="submit" class="btn">RECUPERAR</button>
              <div class="clear"></div>
              <a href="#" class="login__form__esqueciasenha--voltar">VOLTAR AO LOGIN</a>
            </form>
          </div>
          <div class="row d-flex justify-content-center">
            <h3 class="align-self-center">NÃO É CADASTRADO?</h3>
            <button class="btn btn--cadastre-se align-self-center">CADASTRE-SE</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>