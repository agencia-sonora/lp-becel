<section class="comoParticipar section backgroundRadial" id="comoParticipar">
  <div class="container">
    <div class="row">
      <div class="comoParticipar__logo">
        <a href="./" class="nav-link external">
          <picture>
            <img src="resources/media/images/logo-promocao.png" alt="Becel">
          </picture>
        </a>
      </div>
    </div>
    <div class="row row--top-negative">
      <div class="col-sm-7" data-aos="fade-right">
        <picture>
          <img src="resources/media/images/como-participar-premiacoes-semanais.png"
            alt="como participar premiacoes semanais">
        </picture>
      </div>
      <div class="col-sm-5" data-aos="fade-left">
        <div class="comoParticipar__contagem">

          <picture class="comoParticipar__contagem--mobile">
            <img src="resources/media/images/como-participar-mais-de-1000-premios-na-hora.png"
              alt="como participar + de 1000 prêmios na hora">
          </picture>

          <h3>Contagem regressiva para <br>
            <strong> você cadastrar seus cupons.</strong>
          </h3>
          <div id="countdown" data-final="2021/04/05"></div>
          <p>Promoção válida de 01 de março <br>
            a 31 de maio de 2021.</p>
          <picture class="comoParticipar__contagem--desk">
            <img src="resources/media/images/como-participar-mais-de-1000-premios-na-hora.png"
              alt="como participar + de 1000 prêmios na hora">
          </picture>
        </div>
      </div>
    </div>
    <div class="row">
      <a href="#" class="btn btn-lg btn--green btn--green--extralarge btn--center btn--modal">CADASTRE SUAS COMPRAS</a>
    </div>
    <div class="row">
      <h1>COMO PARTICIPAR</h1>
    </div>
    <div class="row">
      <h2>VEJA COMO É FÁCIL CONCORRER A MUITOS PRÊMIOS!</h2>
    </div>
    <div class="row ">
      <ul>
        <li>
          <picture>
            <img src="resources/media/images/como-participar-icon1.png" alt="Compre">
          </picture>
          <p><strong>1. COMPRE</strong> 2 unidades <br>
            de Becel.</p>
        </li>
        <li>
          <picture>
            <img src="resources/media/images/como-participar-icon2.png" alt="Cadastre">
          </picture>
          <p><strong>2. CADASTRE</strong> seu Cupom <br>
            Fiscal <a href="#">aqui.</a></p>
        </li>
        <li>
          <picture>
            <img src="resources/media/images/como-participar-icon3.png" alt="Concora">
          </picture>
          <p><strong>3. CONCORRA </strong> a mais de <span>1.000</span> <br>
            <span>prêmios</span> de até <br>
            <span>r$500,00</span> na hora.
          </p>
        </li>
        <li>
          <picture>
            <img src="resources/media/images/como-participar-icon4.png" alt="Ganhe">
          </picture>
          <p><strong>4. GANHE</strong> números da sorte <br>
            para participar dos <br>
            <span>sorteios semanais.</span>
          </p>
        </li>
      </ul>
    </div>
    <div class="row">
      <a href="#" class="btn btn--green btn--center btn--modal">CADASTRE SUAS COMPRAS</a>
    </div>
  </div>
</section>