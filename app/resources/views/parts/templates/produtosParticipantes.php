<section id="produtosParticipantes" data-aos="fade-top"
  class="produtosParticipantes headlinesSections backgroundRadial section">
  <div class="container">
    <div class="row">
      <h1>PRODUTOS PARTICIPANTES</h1>
    </div>
    <div class="row">
      <div class="owl-carousel owl-carousel--produtosParticipantes owl-theme">

        <div class="item">
          <picture>
            <img src="resources/media/images/Original-CS-500g.png" alt="">
          </picture>
          <div class="owl-carousel--produtosParticipantes__content">
            <h2>Original CS</h2>
            <small>500g</small>
            <p>A número #1 em cuidado do coração</p>
          </div>
        </div>

        <div class="item">
          <picture>
            <img src="resources/media/images/Amanteigado-250g.png" alt="">
          </picture>
          <div class="owl-carousel--produtosParticipantes__content">
            <h2>Amanteigado</h2>
            <small>250g</small>
            <p>Sabor de Manteiga, porém com baixo teor de gordura</p>
          </div>
        </div>

        <div class="item">
          <picture>
            <img src="resources/media/images/Original-SS-500g.png" alt="">
          </picture>
          <div class="owl-carousel--produtosParticipantes__content">
            <h2>Original SS</h2>
            <small>500g</small>
            <p>A número #1 em cuidado do coração</p>
          </div>
        </div>

        <div class="item">
          <picture>
            <img src="resources/media/images/Original-CS-250g.png" alt="">
          </picture>
          <div class="owl-carousel--produtosParticipantes__content">
            <h2>Original CS</h2>
            <small>250g</small>
            <p>A número #1 em cuidado do coração</p>
          </div>
        </div>

        <div class="item">
          <picture>
            <img src="resources/media/images/Amanteigado-500g.png" alt="">
          </picture>
          <div class="owl-carousel--produtosParticipantes__content">
            <h2>Amanteigado</h2>
            <small>500g</small>
            <p>Sabor de Manteiga, porém com baixo teor de gordura</p>
          </div>
        </div>

        <div class="item">
          <picture>
            <img src="resources/media/images/Original-SS-250g.png" alt="">
          </picture>
          <div class="owl-carousel--produtosParticipantes__content">
            <h2>Original SS </h2>
            <small>250g</small>
            <p>A número #1 em cuidado do coração</p>
          </div>
        </div>

        <div class="item">
          <picture>
            <img src="resources/media/images/Pro-Activ.png" alt="">
          </picture>
          <div class="owl-carousel--produtosParticipantes__content">
            <h2>Pro Activ</h2>
            <p>A única Margarina que auxilia na redução do colesterol</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <a href="#" class="btn btn--green btn--center btn--modal">CADASTRE SUAS COMPRAS</a>
    </div>
  </div>
</section>