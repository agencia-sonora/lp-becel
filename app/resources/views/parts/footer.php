<!-- Modals -->
<?php include('./resources/views/parts/templates/login.php');?>
</div>

<footer class="footer footer--bgcolor">
  <div class="container">
    <div class="row d-flex justify-content-start align-items-center">
      <div class="col-10 col-sm-11">
        <aside>
          <ul>
            <li><a href="#">Regulamentos</a></li>
            <li><a href="#">Privacidade</a></li>
            <li><a href="#">Becel</a></li>
          </ul>
        </aside>
        <p>Promoção válida para compras de 02 unidades de Becel no mesmo cupom fiscal realizadas e cadastradas de
          1º/03/2021 a 31/05/2021. *Sorteios semanais conforme data de cadastro. **Participação válida enquanto durarem
          o estoque dos prêmios. Guarde os cupons cadastrados. Consulte as condições de participação e Certificados de
          Autorização nos Regulamentos em www.promocaobecel.com.br. Imagens ilustrativas.</p>
      </div>
      <div class="col-2 col-sm-1 d-flex flex-column">
        <div class="footer__social">
          <a href="#" target="_blank"><i class="fab fa-facebook-square"></i></a>
          <a href="#" target="_blank"><i class="fab fa-instagram"></i></a>
        </div>
      </div>
    </div>
  </div>
</footer>
<script src="scripts.js"></script>
</body>

</html>