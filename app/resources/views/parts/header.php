<!DOCTYPE html>
<html lang="pt_br">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Open Graph / Facebook -->
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:image" content="">

  <!-- Twitter -->
  <meta property="twitter:card" content="summary_large_image">
  <meta property="twitter:url" content="">
  <meta property="twitter:title" content="">
  <meta property="twitter:description" content="">
  <meta property="twitter:image" content="">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="57x57" href="resources/media/icos/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="resources/media/icos/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="resources/media/icos/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="resources/media/icos/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="resources/media/icos/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="resources/media/icos/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="resources/media/icos/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="resources/media/icos/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="resources/media/icos/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="resources/media/icos/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="resources/media/icos/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="resources/media/icos/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="resources/media/icos/favicon-16x16.png">
  <link rel="manifest" href="resources/media/icos/manifest.json">

  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="resources/media/icos/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <!-- Title/Css -->
  <title>Becel</title>
  <link rel="stylesheet" href="https://use.typekit.net/tht8knq.css">
  <link rel="stylesheet" href="styles.css">
  <script src="vendors.js"></script>
</head>

<body>
  <header class="header fixed-top">
    <div class="container">
      <div class="row">
        <div class="header__logo">
          <a href="./" class="nav-link external">
            <img src="resources/media/images/logo.png" alt="Becel">
          </a>
        </div>
      </div>
      <div class="row">
        <nav class="navbar navbar-collapse navbar-expand-lg navbar-light  ">

          <!-- Toggler/collapsibe Button -->
          <button class="navbar-toggler navbar--icon" type="button" data-toggle="collapse"
            data-target="#collapsibleNavbar">
            <span></span>
            <span></span>
            <span></span>
          </button>

          <!-- Button login -->
          <div class="navbar__buttomMobile navbar__buttomMobile--notloged d-flex justify-content-end">
            <a href="#" class="bt-login">LOGIN</a>
          </div>

          <!-- User Container -->
          <!-- <div class="justify-content-end navbar__logged navbar__logged--mobile">
            <small>Olá, Washington</small>
            <ul>
              <li><a href="#"><i class="far fa-user"></i> MINHA ÁREA</a></li>
              <li><a href="#"><i class="far fa-times-circle"></i> SAIR</a></li>
            </ul>
          </div> -->


          <!-- Navbar links -->
          <div class="collapse navbar-collapse justify-content-center" id="collapsibleNavbar">
            <ul class="navbar-nav align-items-center">
              <li class="nav-item nav-item--1">
                <a id="link1" class="nav-link" href="./">HOME</a>
              </li>
              <li class="nav-item nav-item--2">
                <a id="link2" class="nav-link" href="./#comoParticipar">COMO PARTICIPAR</a>
              </li>
              <li class="nav-item nav-item--3">
                <a id="link3" class="nav-link" href="./#premiacao">PREMIAÇÕES</a>
              </li>
              <li class="nav-item nav-item--4">
                <a id="link4" class="nav-link" href="./#produtosParticipantes">PRODUTOS PARTICIPANTES</a>
              </li>
              <li class="nav-item nav-item--5">
                <a id="link5" class="nav-link" href="ganhadores.php">GANHADORES</a>
              </li>
              <li class="nav-item nav-item--6">
                <a id="link6" class="nav-link" href="duvidas.php">DÚVIDAS</a>
              </li>
              <li class="nav-item nav-item--7">
                <a id="link7" class="nav-link" href="contato.php">CONTATO</a>
              </li>
              <li class="header__button nav-item order-1 order-lg-10">
                <div class="container">
                  <div class="row">
                    <!-- Button login -->
                    <div class="col-12 d-flex justify-content-end">
                      <a href="#" class="bt-login">login</a>
                    </div>

                    <!-- User Container -->
                    <!-- <div class=" justify-content-end navbar__logged">
                      <small>Olá, Washington</small>
                      <ul>
                        <li><a href="#"><i class="far fa-user"></i> MINHA ÁREA</a></li>
                        <li><a href="#"><i class="far fa-times-circle"></i> SAIR</a></li>
                      </ul>
                    </div> -->
                  </div>
                </div>
              </li>

            </ul>
          </div>
        </nav>
      </div>
    </div>
  </header>

  <!-- Generic Content -->
  <div class="generic-content">