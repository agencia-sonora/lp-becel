import $ from 'jquery'
import countdown from  'jquery-countdown'

class CountDownCupom{
  constructor(){

    this.countDown = $('#countdown')
    this.countDownMobile = $('#countdownMobile')
    this.dataFinal = $('#countdown').attr("data-final")
    
    this.countDown.countdown(this.dataFinal, function(event){
      var $this = $(this).html(event.strftime('<ul>'
      + '<li><span>%D</span> <small>DIAS</small> </li> '
      + '<li><span>%H</span> <small>HORAS</small></li>  '
      + '<li><span>%M</span> <small>MIN</small></li>  '
      + '</ul>'
      ));
    })

    this.countDownMobile.countdown(this.dataFinal, function(event){
      var $this = $(this).html(event.strftime('<ul>'
      + '<li><span>%D</span> <small>DIAS</small> </li> '
      + '<li><span>%H</span> <small>HORAS</small></li>  '
      + '<li><span>%M</span> <small>MIN</small></li>  '
      + '</ul>'
      ));
    })
  }
  
}

export default CountDownCupom