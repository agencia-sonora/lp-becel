import $ from 'jquery'

class Modal{
  constructor(){
    this.modalLogin = $('.bt-login')
    this.modalCadastre = $('.btn--modal')
    this.modalLoginContainer = $('#modal-login')
    this.buttonRecoverPassword = $('.login__form__esqueciasenha')
    this.buttonBackToLogin = $('.login__form__esqueciasenha--voltar')
    this.formRecoverPassword = $('.login__form--recuperasenha')
    this.formLogin = $('.login__form')
    this.initModal()
    this.events()
  }

  // Events
  events(){
    this.buttonRecoverPassword.on('click', this.ShowFormRecoverPassword.bind(this))
    this.buttonBackToLogin.on('click', this.HideFormRecoverPassword.bind(this))
  }

  // Methods
  initModal(){
    var that = this

    that.modalLogin.on('click',function(e){
      that.modalLoginContainer.modal();
      e.preventDefault()
    })

    that.modalCadastre.on('click',function(e){
      that.modalLoginContainer.modal();
      e.preventDefault()

    })
    
  }
  
  ShowFormRecoverPassword(){
    var that = this
    that.formLogin.css({
      'opacity':'0',
      'display': 'none'
    }).hide().animate({opacity: 0})

    that.formRecoverPassword.css({
      'opacity':'0',
      'display': 'block'
    }).show().animate({opacity: 1})
  }

  HideFormRecoverPassword(){
    var that = this
    that.formLogin.css({
      'opacity':'0',
      'display': 'block'
    }).show().animate({opacity: 1})
    that.formRecoverPassword.css({
      'opacity':'0',
      'display': 'none'
    }).hide().animate({opacity: 0})
  }


}
export default Modal