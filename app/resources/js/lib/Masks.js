import $ from 'jquery'
import mask from 'jquery-mask-plugin'

class MaskField{
  constructor(){
    this.cpfField = $('#login-cpf')
    this.cpfFieldRecover = $('#recupere-cpf')
    this.cadCelular = $('#cad-celular')
    this.cadCPF = $('#cad-cpf')
    this.cadNascimento = $('#cad-dnascimento')
    this.cadCEP = $('#cad-cep')
    this.cadCNPJ = $('#cad-cnpj')
    this.MascaraCampos()
  }

  // Methods
  MascaraCampos(){
    var that = this

    $(that.cpfField).mask('000.000.000-00', {reverse: true})
    $(that.cpfFieldRecover).mask('000.000.000-00', {reverse: true})
    $(that.cadCelular).mask('(00) 00000-0000', {reverse: false, placeholder: "(__) _____-____"})
    $(that.cadCPF).mask('000.000.000-00', {reverse: true})
    $(that.cadNascimento).mask("00/00/0000", {placeholder: "__/__/____"})
    $(that.cadCEP).mask("00000-000", {placeholder: '00000-000'})
    $(that.cadCNPJ).mask("00.000.000/0000-00", {placeholder: '00.000.000/0000-00'})
  }
}
export default MaskField