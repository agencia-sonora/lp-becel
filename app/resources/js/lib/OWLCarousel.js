import $ from 'jquery'
import owlcarousel from 'owl.carousel'

class OWL {
  constructor(){
    this.carouselProdutosRelacionados = $('.owl-carousel--produtosParticipantes')
    this.carouselCadastroCompras = $('.owl-carousel--cadastroCompras')
    this.initCarouselRelacionados()
  }

  // methods
  initCarouselRelacionados(){
    var that = this

    if(this.carouselProdutosRelacionados){
      that.carouselProdutosRelacionados.owlCarousel({
        loop: true,
        items: 3,
        margin: -80,
        nav: true,
        dots: false,
        responsiveClass: true,
        center: true,
        responsive: {
          0:{
            items: 1,
            loop: false,
            margin: 0,
          },
          600: {
            items: 1,
            loop: false,
            margin: 0,
          },
          1000: {
            items: 3,
            loop: true,
            margin: -80,
          }
        }
      })
    }

    if(this.carouselCadastroCompras){
      that.carouselCadastroCompras.owlCarousel({
        items: 3,
        nav: false,
        dots: false,
        responsiveClass: true,
        margin: 10,
        responsive: {
          0:{
            items: 1,
            loop: false,
            margin: 0,
            nav: true,
          },
          600: {
            items: 1,
            loop: false,
            margin: 0,
            nav: true,
          },
          1000: {
            items: 3,
            loop: false,
            margin: -271,
          }
        }
      })
    }

  }
}

export default OWL