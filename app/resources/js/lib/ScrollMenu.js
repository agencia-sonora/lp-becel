import $ from 'jquery'
import smoothScroll from 'jquery-smooth-scroll/src/jquery.smooth-scroll'

class Scroll{
    constructor(){
        this.ScrollButton = $('.nav-link')
        this.headerContainer = $('.header')
        this.addSmoothScrolling()
        this.showAndHideLogoHeader()
    }
    
    addSmoothScrolling(){
      var that = this
      that.ScrollButton.smoothScroll({
        offset: -100,
        afterScroll: function(e) {
          console.log(e.link)
          console.log(e.scrollTarget)
          that.ScrollButton.not($(this)).removeClass('active')
          $(this).toggleClass('active')
        }
      });
    }

    showAndHideLogoHeader(){

      var that = this
      
      $(window).scroll(function(){

        if($(this).scrollTop() > 50){
          that.headerContainer.addClass('header--no-logo')
        } else {
          that.headerContainer.removeClass('header--no-logo')
        }

      })
    }
}
export default Scroll