import $, { event } from 'jquery'
import JQVMAP from 'jqvmap-novulnerability/dist/jquery.vmap.min'
import 'jqvmap-novulnerability/dist/maps/jquery.vmap.brazil'

class MAP{
  constructor(){
    this.regiaoAC = $('#AC').html()
    this.regiaoAM = $('#AM').html()
    this.regiaoRO = $('#RO').html()
    this.regiaoMT = $('#MT').html()
    this.regiaoRR = $('#RR').html()
    this.regiaoPA = $('#PA').html()
    this.regiaoAP = $('#AP').html()
    this.regiaoMA = $('#MA').html()
    this.regiaoPI = $('#PI').html()
    this.regiaoTO = $('#TO').html()
    this.regiaoMS = $('#MS').html()
    this.regiaoGO = $('#GO').html()
    this.regiaoDF = $('#DF').html()
    this.regiaoBA = $('#BA').html()
    this.regiaoPE = $('#PE').html()
    this.regiaoCN = $('#CN').html()
    this.regiaoPB = $('#PB').html()
    this.regiaoAL = $('#AL').html()
    this.regiaoCE = $('#CE').html()
    this.regiaoSE = $('#SE').html()
    this.regiaoMG = $('#MG').html()
    this.regiaoES = $('#ES').html()
    this.regiaoRJ = $('#RJ').html()
    this.regiaoSP = $('#SP').html()
    this.regiaoPR = $('#PR').html()
    this.regiaoSC = $('#SC').html()
    this.regiaoRS = $('#RS').html()
    this.regiaoRN = $('#RN').html()
    this.initMap()
    
  }
  
  // Methods
  initMap(){
    
    var that = this
    
    var regions = ['ac', 'am', 'ro', 'mt', 'rr', 'pa', 'ap', 'ma', 'pi', 'to', 'ms', 'go', 'df', 'ba', 'pe', 'cn', 'pb', 'al', 'ce', 'se', 'mg', 'es', 'rj', 'sp', 'pr', 'sc', 'rs', 'rn'];
    var data = {
      ac: `${this.regiaoAC}`,
      am: `${this.regiaoAM}`,
      ro: `${this.regiaoRO}`,
      mt: `${this.regiaoMT}`,
      rr: `${this.regiaoRR}`,
      pa: `${this.regiaoPA}`,
      ap: `${this.regiaoAP}`,
      ma: `${this.regiaoMA}`,
      pi: `${this.regiaoPI}`,
      to: `${this.regiaoTO}`,
      ms: `${this.regiaoMS}`,
      go: `${this.regiaoGO}`,
      df: `${this.regiaoDF}`,
      ba: `${this.regiaoBA}`,
      pe: `${this.regiaoPE}`,
      cn: `${this.regiaoCN}`,
      pb: `${this.regiaoPB}`,
      al: `${this.regiaoAL}`,
      ce: `${this.regiaoCE}`,
      se: `${this.regiaoSE}`,
      mg: `${this.regiaoMG}`,
      es: `${this.regiaoES}`,
      rj: `${this.regiaoRJ}`,
      sp: `${this.regiaoSP}`,
      pr: `${this.regiaoPR}`,
      sc: `${this.regiaoSC}`,
      rs: `${this.regiaoRS}`,
      rn: `${this.regiaoRN}`
    };
    
    // function escapeXml(string) {
    //   return string.replace(/[<>]/g, function (c) {
    //     switch (c) {
    //       case '<': return '\u003c';
    //       case '>': return '\u003e';
    //     }
    //   });
    // }
    
    // var pinsClass = '<div class="map-pin"><span></span></div>'
    // var pins = {
    //   am: escapeXml(pinsClass),
    //   pa: escapeXml(pinsClass),
    //   mt: escapeXml(pinsClass),
    //   ba: escapeXml(pinsClass)
    // }
    
    $(document).ready(function() {
      
      $('#vmap').vectorMap({
        map: 'brazil_br',
        backgroundColor: 'transparent',
        borderColor: '#fff',
        borderOpacity: 1,
        borderWidth: 1,
        color: '#e1cc59',
        pins: pins,
        pinMode: 'content',
        enableZoom: false,
        hoverColor: '#F5CF40',
        scaleColors: ['#b6d6ff', '#005ace'],
        selectedColor: '#F5CF40',
        showTooltip: false,
        onRegionClick: function(element, code, region) {
          $("#region").html(data[code]);
          $("#region").show();
        }
        
      })
    })
  }
}
export default MAP