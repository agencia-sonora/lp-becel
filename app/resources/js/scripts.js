import 'bootstrap'

// Classes
import AOSScrollAnimate from './lib/AOSAnimate'
import MascaraCampos from './lib/Masks'
import Modal from './lib/Modal'
import CountDown from './lib/Countdown'
import OWL from './lib/OWLCarousel'
import SCROLL from './lib/ScrollMenu'
import MAP from './lib/Vmap'

// Objetcs
var aosanimate = new AOSScrollAnimate()
var mascaracampos = new MascaraCampos()
var modal = new Modal()
var countdown = new CountDown()
var owl = new OWL()
var scroll = new SCROLL()
var vmap = new MAP()

// Reusable code
$(document).ready( function(){

// Menu hamburguer
$('.navbar-toggler').on('click', function(){
  $(this).toggleClass('open')
})


});