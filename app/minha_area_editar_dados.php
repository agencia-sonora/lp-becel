<!-- Header -->
<?php include('./resources/views/parts/header.php');?>

<section id="cadastre-se" class="section cadastre-se backgroundRadial">
  <div class="container">
    <div class="row">
      <h1>EDITAR MEUS DADOS CADASTRADOS</h1>
    </div>
    <div class="row">
      <form method="post" action="" class="form form--inner form--inner--cadastre-se">
        <div class="form-group">
          <label for="cad-cpf">CPF</label>
          <input id="cad-cpf" class="form-control form-control-lg " type="text" name="register_cpf">
        </div>
        <div class="form-group">
          <label for="cad-nomecompleto">Nome completo</label>
          <input id="cad-nomecompleto" class="form-control form-control-lg" type="text" name="register_nome">
        </div>
        <div class="form-row">
          <div class="form-group col-12 col-sm-4">
            <label for="cad-celular">Telefone celular</label>
            <input id="cad-celular" class="form-control" type="text" name="register_celular">
          </div>
          <div class="form-group col-12 col-sm-3">
            <label for="cad-dnascimento">Data de nascimento</label>
            <input id="cad-dnascimento" class="form-control" type="text" name="register_dnascimento">
          </div>
          <div class="form-group col-12 col-sm-3">
            <label for="cad-sexo">Sexo:</label>
            <select name="sexo" id="cad_sexo">
              <option value="">Selecione</option>
              <option value="Masculino">Masculino</option>
              <option value="Feminino">Feminino</option>
              <option value="Prefiro não informar">Prefiro não informar</option>
            </select>
          </div>
          <div class="form-group col-12 col-sm-2">
            <label for="cad-cep">CEP</label>
            <input id="cad-cep" class="form-control" type="text" name="register_cep">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-12 col-sm-4">
            <label for="cad-uf">UF</label>
            <input id="cad-uf" class="form-control" type="text" name="register_uf">
          </div>
          <div class="form-group col-12 col-sm-4">
            <label for="cad-cidade">Cidade</label>
            <input id="cad-cidade" class="form-control" type="text" name="register_cidade">
          </div>
          <div class="form-group col-12 col-sm-4">
            <label for="cad-bairro">Bairro</label>
            <input id="cad-bairro" class="form-control" type="text" name="register_bairro">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-12 col-sm-6">
            <label for="cad-email">E-mail</label>
            <input id="cad-email" class="form-control" type="text" name="register_email">
          </div>
          <div class="form-group col-12 col-sm-6">
            <label for="cad-confirmaemail">Confirmar e-mail</label>
            <input id="cad-confirmaemail" class="form-control" type="text" name="register_verifyemail">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-12 col-sm-6">
            <label for="cad-senha">Cadastre sua senha</label>
            <input id="cad-senha" class="form-control" type="password" name="register_password">
            <small>Sua senha deve conter de 6 a 10 caracteres.</small>
          </div>
          <div class="form-group col-12 col-sm-6">
            <label for="cad-senha">Confirme sua senha</label>
            <input id="cad-senha" class="form-control" type="password" name="register_password">
          </div>
        </div>
        <div class="form-row">
          <p>*Aceito receber novidades e informações da promoção e da Upfield via E-mail.</p>
        </div>
        <div class="form-row">
          <div class="form-check form-check-inline">
            <input id="user_optin_mail" class="form-check-input" type="radio" name="optradio" value="1">
            <label for="user_optin_mail" class="form-check-label radio">Sim</label>
          </div>
          <div class="form-check form-check-inline">
            <input id="user_optin_notmail" class="form-check-input" type="radio" name="optradio" value="0">
            <label for="user_optin_notmail" class="form-check-label radio">Não</label>
          </div>
        </div>
        <div class="form-row">
          <br>
          <button type="submit" class="btn btn-lg align-self-center">ALTERAR DADOS</button>
        </div>
        <div class="form-row form-row--excluir">
          <p>
            <strong>Excluir cadastro</strong> <br>
            Ao clicar no botão abaixo seus dados serão apagados de nosso sistema em até 72 horas e você não estará mais
            participando
            nem concorrendo a nenhuma categoria de premiação. Caso deseje voltar a participar você poderá se cadastrar
            novamente.
          </p>
          <br>
        </div>
        <div class="form-row">
          <button type="submit" class="btn btn-lg align-self-center">APAGAR CONTA</button>
        </div>
      </form>
    </div>
  </div>
</section>

<!-- Footer -->
<?php include('./resources/views/parts/footer.php');?>