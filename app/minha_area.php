<?php include('./resources/views/parts/header.php');?>

<section class="minhaArea section backgroundRadial">
  <div class="container">
    <div class="row">
      <h1>MINHA ÁREA</h1>
    </div>
    <div class="row">
      <p><strong>Atenção:</strong> todas as comunicações serão enviadas para o seu e-mail cadastrado. Caso não tenha
        recebido, confira sua caixa de SPAM.</p>
    </div>
    <div class="row">
      <div class="minhaArea__banner">
        <picture>
          <img class="center" src="resources/media/images/Minha_Area_banner.jpg"
            alt="Concorra TODA Semana!  Bicicletas elétricas e iphone 12">
        </picture>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="card">
          <div class="card-header">
            <strong>DADOS CADASTRADOS</strong>
          </div>
          <div class="card-body">
            <p class="card-text">NOME: <strong>CAMILLE LOREN IPSUN</strong></p>
            <p class="card-text">DATA DE NASCIMENTO: <strong>12/12/1981</strong></p>
            <p class="card-text">E-MAIL: <strong>camille@gmail.com.br</strong></p>
            <p class="card-text">CELULAR: <strong>11 91234-5678</strong></p>
            <p class="card-text">UF: <strong>SP</strong></p>
            <a href="#" class="btn btn--green--full btn--green">EDITAR MEUS DADOS</a>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="minhaArea__contagem">
          <br><br>
          <p>Contagem regressiva para <br>
            <strong> você cadastrar seus cupons.</strong>
          </p>
          <div id="countdown" class="countdown--blue"></div>
          <a href="#" class="btn btn--green btn--center">CADASTRE SUAS COMPRAS</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="card">
          <div class="card-header">
            <strong>CUPONS OU NOTAS <br>
              FISCAIS CADASTRADAS</strong>
          </div>
          <div class="card-body">
            <br>
            <h3>VOCÊ AINDA NÃO POSSUI <br>
              CUPONS CADASTRADOS</h3>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card">
          <div class="card-header">
            <strong>MEUS NÚMEROS DA SORTE</strong> <br>
            <small>VOCÊ TEM XX NÚMEROS DA SORTE</small>
          </div>
          <div class="card-body">
            <br>
            <h3>VOCÊ AINDA NÃO POSSUI <br>
              NÚMEROS DA SORTE</h3>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <h2>MEUS PRÊMIOS INSTANTÂNEOS</h2>
    </div>
    <div class="row">
      <h3>VOCÊ AINDA NÃO CONQUISTOU PRÊMIOS INSTANTÂNEOS.</h3>
    </div>
    <div class="row">
      <div class="minhaArea__premios minhaArea__premios--semanal">
        <h2>MEUS PRÊMIOS SEMANAIS</h2>
        <h3>VOCÊ AINDA NÃO CONQUISTOU PRÊMIOS SEMANAIS.</h3>
      </div>
    </div>
  </div>
</section>

<?php include('./resources/views/parts/footer.php');?>