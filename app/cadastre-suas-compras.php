<?php include('./resources/views/parts/header.php');?>

<section id="cadastreSuasCompras" class="cadastreSuasCompras section backgroundRadial">
  <div class="container justify-content-center">
    <div class="row">
      <h1>CADASTRE SUAS COMPRAS</h1>
    </div>
    <div class="row">
      <p>Preencha o formulário abaixo com os dados do seu Cupom ou Nota Fiscal e faça o upload da imagem. </p>
    </div>
    <div class="row">
      <form method="post" action="" class="form form--inner form--inner--compras">
        <div class="form-row">
          <div class="form-group col-lg-6">
            <label for="cad-cnpj">Informe o CNPJ do estabelecimento.</label>
            <input id="cad-cnpj" class="form-control form-control-lg " type="text" name="register_cnpj">
          </div>
          <div class="form-group col-lg-6">
            <label for="cad-cupom">Insira o número do Cupom Fiscal.</label>
            <input id="cad-cupom" class="form-control form-control-lg " type="text" name="register_cupom">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-lg-6">
            <label for="cad-produtos">informe a quantidade de produtos Becel.</label>
            <select id="cad-produtos" class="form-control form-control-lg" name="register_produtos">
              <option selected>Selecione</option>
              <option value="Produtos participantes">05</option>
              <option value="Período da promoção">10</option>
              <option value="Premiação">15</option>
              <option value="Cupons ou Notas Fiscais">20</option>
              <option value="Limite de números da sorte">25</option>
              <option value="Outros">Outros</option>
            </select>
          </div>
          <div class="form-group col-lg-6">
            <button type="submit" class="btn btn-lg align-self-center">CLIQUE AQUI E VEJA ONDE ENCONTRAR
              OS DADOS EM SEU CUPOM FISCAL.</button>
          </div>
        </div>
      </form>
    </div>
    <div class="row">
      <picture>
        <img src="resources/media/images/cadastre-suas-compras-.png" alt="Cadastre suas compras">
      </picture>
    </div>
    <div class="row">
      <p>&nbsp;</p>
    </div>
    <div class="row">
      <p>SAIBA COMO ENVIAR AS FOTOS DO SEU CUPOM OU NOTA FISCAL CORRETAMENTE.</p>
      <div class="owl-carousel owl-theme owl-carousel--cadastroCompras">
        <div class="item">
          <img src="resources/media/images/saiba_1.png" alt="">
        </div>
        <div class="item">
          <img src="resources/media/images/saiba_2.png" alt="">
        </div>
        <div class="item">
          <img src="resources/media/images/saiba_3.png" alt="">
        </div>
      </div>
    </div>
    <div class="row">
      <form method="post" action="" class="form form--inner form--inner--upload">
        <div class="form-row">
          <div class="col-sm-6">
            <br><br>
            <picture>
              <img src="resources/media/images/faca-o-upload-do-seu-cupom-ou-nota-fiscal.png" alt="">
            </picture>
            <input type="file" class="btn btn--green btn--green--upload align-self-center" id="uploadFile"
              name="filename">
            <br><br>
          </div>
          <div class="col-sm-6">
            <picture>
              <img class="center" src="resources/media/images/upload.png" alt="Imagem preview">
            </picture>
            <button type="button" class="btn btn--green align-self-center">TROCAR IMAGEM</button>
          </div>
        </div>
        <div class="form-row">
          <p>&nbsp;</p>
        </div>
        <div class="form-row">
          <h2>ANTES DE FINALIZAR, TENHA CERTEZA DE QUE SEU CUPOM OU NOTA FISCAL
            É VÁLIDO PARA PARTICIPAÇÃO DOS SORTEIOS. </h2>
          <p>1º - A imagem do seu Cupom ou Nota Fiscal deve ser legível e garantir a leitura das informações presentes
            no cabeçalho <br>
            (nome da loja e data da compra), assim como os produtos adquiridos.</p>
          <p>2º - Não é permitido o cadastro do mesmo Cupom ou Nota Fiscal mais de uma vez. Caso isso aconteça, só será
            considerado <br>
            o 1º número da sorte emitido.</p>
          <p>3º - Só são permitidos os arquivos de imagem de Cupons e Notas Fiscais nas extensões JPG ou PNG e no
            tamanho máximo de 4MB.</p>
          <p>4º - Lembre-se de guardar seu Cupom ou Nota Fiscal até o final da promoção. O documento será requisitado no
            momento <br>
            de validar sua premiação.</p>
          <p>5º - Certifique-se de que o Cupom ou Nota Fiscal cadastrado conta com o mesmo CPF informado no site ou que
            <br>
            o comprovante fiscal esteja sem indicação de CPF.
          </p>
        </div>
        <div class="form-row">
          <p>&nbsp;</p>
        </div>
        <div class="form-row">
          <div class="form-check form-check-inline">
            <input id="cad-concordo-cupom" class="form-check-input" type="checkbox" name="" value="true">
            <label for="cad-concordo-cupom" class="form-check-label checkbox">Li e concordo com as condições
              acima para validar meu cupom.</label>
          </div>
        </div>
        <div class="form-row">
          <p>&nbsp;</p>
        </div>
        <div class="form-row">
          <button type="submit" class="btn btn--green align-self-center">Prosseguir e cadastrar Cupom ou Nota
            Fiscal</button>
        </div>
      </form>

    </div>
  </div>
</section>

<?php include('./resources/views/parts/footer.php');?>