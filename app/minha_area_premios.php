<?php include('./resources/views/parts/header.php');?>

<section class="minhaArea section backgroundRadial">
  <div class="container">
    <div class="row">
      <h1>MINHA ÁREA</h1>
    </div>
    <div class="row">
      <p><strong>Atenção:</strong> todas as comunicações serão enviadas para o seu e-mail cadastrado. Caso não tenha
        recebido, confira sua caixa de SPAM.</p>
    </div>
    <div class="row">
      <div class="minhaArea__banner">
        <picture>
          <img class="center" src="resources/media/images/Minha_Area_banner.jpg"
            alt="Concorra TODA Semana!  Bicicletas elétricas e iphone 12">
        </picture>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="card">
          <div class="card-header">
            <strong>DADOS CADASTRADOS</strong>
          </div>
          <div class="card-body">
            <p class="card-text">NOME: <strong>CAMILLE LOREN IPSUN</strong></p>
            <p class="card-text">DATA DE NASCIMENTO: <strong>12/12/1981</strong></p>
            <p class="card-text">E-MAIL: <strong>camille@gmail.com.br</strong></p>
            <p class="card-text">CELULAR: <strong>11 91234-5678</strong></p>
            <p class="card-text">UF: <strong>SP</strong></p>
            <a href="#" class="btn btn--green--full btn--green">EDITAR MEUS DADOS</a>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="minhaArea__contagem">
          <br><br>
          <p>Contagem regressiva para <br>
            <strong> você cadastrar seus cupons.</strong>
          </p>
          <div id="countdown" data-final="2021/04/05" class="countdown--blue"></div>
          <a href="#" class="btn btn--green btn--center btn--modal">CADASTRE SUAS COMPRAS</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="card">
          <div class="card-header">
            <strong>CUPONS OU NOTAS <br>
              FISCAIS CADASTRADAS</strong>
          </div>
          <div class="card-body">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <th width="40%">Código do Cupom <br>
                    ou Nota Fiscal</th>
                  <th width="30%">Data de <br>
                    emissão</th>
                  <th width="30%">Quantidade</th>
                </tr>
                <tr class="flag">
                  <td colspan="3">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="40%">282829</td>
                        <td width="30%">12/07/20</td>
                        <td width="30%">2 PRODUTOS</td>
                      </tr>
                    </table>
                    <p>VOCÊ JÁ CONQUISTOU XX PRÊMIOS INSTANTÂNEOS</p>
                  </td>
                </tr>
                <tr>
                  <td>282829</td>
                  <td>12/07/20</td>
                  <td>2 PRODUTOS</td>
                </tr>
                <tr>
                  <td>282829</td>
                  <td>12/07/20</td>
                  <td>2 PRODUTOS</td>
                </tr>
                <tr>
                  <td>282829</td>
                  <td>12/07/20</td>
                  <td>2 PRODUTOS</td>
                </tr>

              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card">
          <div class="card-header">
            <strong>MEUS NÚMEROS DA SORTE</strong> <br>
            <small>VOCÊ TEM XX NÚMEROS DA SORTE</small>
          </div>
          <div class="card-body">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                </tr>
                <tr>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                </tr>
                <tr>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                </tr>
                <tr>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                </tr>
                <tr>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                  <td align="center">282829</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="minhaArea__contagem minhaArea__contagem--mobile">
        <br><br>
        <p>Contagem regressiva para <br>
          <strong> você cadastrar seus cupons.</strong>
        </p>
        <div id="countdownMobile" data-final="2021/04/05" class="countdown--blue"></div>
        <a href="#" class="btn btn--green btn--center btn--modal">CADASTRE SUAS COMPRAS</a>
      </div>
    </div>
    <div class="row">
      <h2>MEUS PRÊMIOS INSTANTÂNEOS</h2>
    </div>
    <div class="row">
      <h3>VOCÊ JÁ CONQUISTOU XX PRÊMIOS INSTANTÂNEOS.</h3>
    </div>
    <div class="row">
      <div class="minhaArea__premios">
        <table class="minhaArea__premios__cabecalho" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td width="40%">CÓDIGO DO CUPOM FISCAL</td>
              <td width="40%" align="center">PREMIAÇÃO</td>
              <td width="20%">STATUS</td>
            </tr>
          </tbody>
        </table>
        <table class="minhaArea__premios__invalido" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td width="40%">282829</td>
              <td width="40%" align="center">IFOOD - R$ 50,00</td>
              <td width="20%" align="center"><img src="resources/media/images/invalido.png" alt="inválido"></td>
            </tr>
          </tbody>
        </table>
        <table class="minhaArea__premios__invalido__venceu" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td>CUPOM EMITIDO FORA DO PERÍODO DA PROMOÇÃO.</td>
            </tr>
          </tbody>
        </table>
        <table class="minhaArea__premios__analise" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td width="40%">282829</td>
              <td width="40%" align="center">GLOBOPLAY - 3 MESES</td>
              <td width="20%" align="center"><img src="resources/media/images/analise.png" alt="Análise"></td>
            </tr>
            <tr>
              <td width="40%">282829</td>
              <td width="40%" align="center">GLOBOPLAY - 3 MESES</td>
              <td width="20%" align="center"><img src="resources/media/images/analise.png" alt="Análise"></td>
            </tr>
          </tbody>
        </table>
        <table class="minhaArea__premios__premiado" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td width="40%">282829</td>
              <td width="40%" align="center">NETSHOES - R$ 500,00</td>
              <td width="20%" align="center"><img src="resources/media/images/premiado.png" alt="Premiado"></td>
            </tr>
          </tbody>
        </table>
        <table class="minhaArea__premios__premiado__aviso" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td width="100%">CUPOM EMITIDO FORA DO PERÍODO DA PROMOÇÃO.</td>
            </tr>
          </tbody>
        </table>
        <table class="minhaArea__premios__premiado" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td width="40%">282829</td>
              <td width="40%" align="center">DEEZER - 12 MESES</td>
              <td width="20%" align="center"><img src="resources/media/images/premiado.png" alt="Premiado"></td>
            </tr>
          </tbody>
        </table>
        <table class="minhaArea__premios__premiado__aviso" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td>Código do vale-compras: 65423156113. Saiba como utilizar.</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <ul>
        <li>
          <img src="resources/media/images/premiado.png" alt="Premiado"> PREMIADO
        </li>
        <li>
          <img src="resources/media/images/analise.png" alt="Análise"> EM ANÁLISE
        </li>
        <li>
          <img src="resources/media/images/invalido.png" alt="inválido"> INVÁLIDO
        </li>
      </ul>
      <p>A VALIDAÇÃO DOS PRÊMIOS SERÁ REALIZADA EM ATÉ 7 DIAS ÚTEIS.</p>
    </div>
    <div class="row">
      <div class="minhaArea__premios minhaArea__premios--semanal">
        <h2>MEUS PRÊMIOS SEMANAIS</h2>
        <table class="minhaArea__premios__cabecalho" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td width="40%">CÓDIGO DO CUPOM FISCAL</td>
              <td width="40%" align="center">PREMIAÇÃO</td>
              <td width="20%">STATUS</td>
            </tr>
          </tbody>
        </table>
        <table class="minhaArea__premios__premiado" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td width="40%">282829</td>
              <td width="40%" align="center">IPHONE 12</td>
              <td width="20%" align="center"><img src="resources/media/images/premiado.png" alt="Premiado"></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>

<?php include('./resources/views/parts/footer.php');?>