<?php include('./resources/views/parts/header.php');?>

<section id="contato" class="contato section section--7 backgroundRadial">
  <div class="container justify-content-center">
    <div class="row">
      <h1>CONTATO</h1>
      <h2>FICOU COM ALGUMA DÚVIDA OU DESEJA FALAR COM A GENTE? <br>
        <strong>PREENCHA OS DADOS ABAIXO E MANDE SUA MENSAGEM.</strong>
      </h2>
    </div>
    <div class="row">
      <form method="post" action="" class="form form--inner form--inner--contato">
        <div class="form-group">
          <label for="cad-nome">Nome Completo</label>
          <input id="cad-nome" class="form-control form-control-lg" type="text" name="contact_nome">
        </div>
        <div class="form-row">
          <div class="form-group col-sm-8">
            <label for="cad-email">E-mail</label>
            <input id="cad-email" class="form-control form-control-lg" type="text" name="contact_email">
          </div>
          <div class="form-group col-sm-4">
            <label for="cad-celular">Celular</label>
            <input id="cad-celular" class="form-control form-control-lg" type="text" name="contact_celular">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-12">
            <label for="cad-assunto">Assunto</label>
            <select id="cad-assunto" class="form-control form-control-lg" name="contact_assunto">
              <option selected>Selecione</option>
              <option value="Produtos participantes">Produtos participantes</option>
              <option value="Período da promoção">Período da promoção</option>
              <option value="Premiação">Premiação</option>
              <option value="Cupons ou Notas Fiscais">Cupons ou Notas Fiscais</option>
              <option value="Limite de números da sorte">Limite de números da sorte
              </option>
              <option value="Outros">Outros</option>
            </select>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-12">
            <label for="cad-mensagem" class="center">Mensagem</label>
            <textarea id="cad-mensagem" class="form-control form-control-lg" name="contact_mensagem"
              rows="4"></textarea>
          </div>
        </div>
        <small>Os dados pessoais enviados neste formulário serão apenas mantidos com a finalidade de responder
          as suas perguntas e não serão usados para propósitos de marketing. </small>
        <button type="submit" class="btn btn-lg align-self-center">ENVIAR</button>
      </form>

    </div>
  </div>
</section>

<?php include('./resources/views/parts/footer.php');?>