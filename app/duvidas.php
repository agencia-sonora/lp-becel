<?php include('./resources/views/parts/header.php');?>

<section id="duvidas" class="section duvidas backgroundRadial">
  <div class="container">
    <div class="row">
      <h1>DÚVIDAS</h1>
      <h2>CONFIRA AS PERGUNTAS E RESPOSTAS FREQUENTES.</h2>
    </div>
    <div class="row">
      <div id="accordion" class="col-12">
        <div class="card" data-aos="fade-up">
          <div class="card-header" id="headingOne">
            <div class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false"
              aria-controls="collapseOne">
              <h5 class="mb-0 d-flex align-items-center">
                <span class="number">
                  01
                </span>
                <span class="quest">
                  Qual o período de participação?
                </span>
                <span class="plus ml-auto">
                  <i class="fas fa-plus"></i>
                </span>
              </h5>
            </div>

          </div>
          <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf
              moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
              Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
              shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea
              proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
              aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
          </div>
        </div>
        <div class="card" data-aos="fade-up">
          <div class="card-header" id="headingTwo">
            <div class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
              aria-controls="collapseTwo">
              <h5 class="mb-0 d-flex align-items-center">
                <span class="number">
                  02
                </span>
                <span class="quest">
                  Como faço para me cadastrar?
                </span>
                <span class="plus ml-auto">
                  <i class="fas fa-plus"></i>
                </span>
              </h5>
            </div>
          </div>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
            <div class="card-body">
              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf
              moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
              Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
              shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea
              proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
              aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
          </div>
        </div>
        <div class="card" data-aos="fade-up">
          <div class="card-header" id="headingThree">
            <div class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree"
              aria-expanded="false" aria-controls="collapseThree">
              <h5 class="mb-0 d-flex align-items-center">
                <span class="number">
                  03
                </span>
                <span class="quest">
                  Quem pode participar desta promoção?
                </span>
                <span class="plus ml-auto">
                  <i class="fas fa-plus"></i>
                </span>
            </div>
            </h5>
          </div>
          <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
            <div class="card-body">
              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf
              moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
              Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
              shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea
              proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
              aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
          </div>
        </div>
        <div class="card" data-aos="fade-up">
          <div class="card-header" id="headingFour">
            <div class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false"
              aria-controls="collapseFour">
              <h5 class="mb-0 d-flex align-items-center">
                <span class="number">
                  04
                </span>
                <span class="quest">
                  Como faço para participar?
                </span>
                <span class="plus ml-auto">
                  <i class="fas fa-plus"></i>
                </span>
              </h5>
            </div>
          </div>
          <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
            <div class="card-body">
              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf
              moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
              Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
              shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea
              proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
              aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
          </div>
        </div>
        <div class="card" data-aos="fade-up">
          <div class="card-header" id="headingFive">

            <div class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false"
              aria-controls="collapseFive">
              <h5 class="mb-0 d-flex align-items-center">
                <span class="number">
                  05
                </span>
                <span class="quest">
                  Quais os produtos que PARTICIPAM desta promoção?
                </span>
                <span class="plus ml-auto">
                  <i class="fas fa-plus"></i>
                </span>
              </h5>
            </div>

          </div>
          <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
            <div class="card-body">
              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf
              moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
              Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
              shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea
              proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
              aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include('./resources/views/parts/footer.php');?>