<?php include('./resources/views/parts/header.php');?>

<section class="roleta section">
  <div class="container">
    <div class="row ">
      <div class="roleta__desk d-flex">
        <div class="col-sm-6 align-items-stretch align-self-center">
          <h1>SÃO MAIS DE 1.000 PRÊMIOS
            DE ATÉ R$ 500,00 PARA VOCÊ
            CONQUISTAR NA HORA!</h1>
          <h2>VOCÊ PODE GIRAR A ROLETA X VEZES</h2>
          <a href="#" class="btn btn--green btn--center btn--green--mediumlarge">GIRE A ROLETA</a>
        </div>
        <div class="col-sm-6 align-items-stretch">
          <picture>
            <img src="resources/media/images/roleta.png" alt="Roleta">
          </picture>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="roleta__mobile">
        <h1>SÃO MAIS DE 1.000 PRÊMIOS
          DE ATÉ R$ 500,00 PARA VOCÊ
          CONQUISTAR NA HORA!</h1>
        <picture>
          <img src="resources/media/images/roleta.png" alt="Roleta">
        </picture>
        <h2>VOCÊ PODE GIRAR A ROLETA X VEZES</h2>
        <a href="#" class="btn btn--green btn--center btn--green--mediumlarge">GIRE A ROLETA</a>

      </div>
    </div>
  </div>
</section>

<?php include('./resources/views/parts/footer.php');?>