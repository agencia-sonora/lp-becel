<!-- Header -->
<?php include('./resources/views/parts/header.php');?>

<section id="cadastre-se" class="section cadastre-se backgroundRadial">
  <div class="container">
    <div class="row">
      <h1>CADASTRE-SE E CONCORRA A PRÊMIOS INSTANTÂNEOS E SEMANAIS</h1>
    </div>
    <div class="row">
      <form method="post" action="" class="form form--inner form--inner--cadastre-se">
        <div class="form-group">
          <label for="cad-cpf">CPF</label>
          <input id="cad-cpf" class="form-control form-control-lg " type="text" name="register_cpf">
        </div>
        <div class="form-group">
          <label for="cad-nomecompleto">Nome completo</label>
          <input id="cad-nomecompleto" class="form-control form-control-lg" type="text" name="register_nome">
        </div>
        <div class="form-row">
          <div class="form-group col-12 col-sm-4">
            <label for="cad-celular">Telefone celular</label>
            <input id="cad-celular" class="form-control" type="text" name="register_celular">
          </div>
          <div class="form-group col-12 col-sm-3">
            <label for="cad-dnascimento">Data de nascimento</label>
            <input id="cad-dnascimento" class="form-control" type="text" name="register_dnascimento">
          </div>
          <div class="form-group col-12 col-sm-3">
            <label for="cad-sexo">Sexo:</label>
            <select name="sexo" id="cad_sexo">
              <option value="">Selecione</option>
              <option value="Masculino">Masculino</option>
              <option value="Feminino">Feminino</option>
              <option value="Prefiro não informar">Prefiro não informar</option>
            </select>
          </div>
          <div class="form-group col-12 col-sm-2">
            <label for="cad-cep">CEP</label>
            <input id="cad-cep" class="form-control" type="text" name="register_cep">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-12 col-sm-4">
            <label for="cad-uf">UF</label>
            <input id="cad-uf" class="form-control" type="text" name="register_uf">
          </div>
          <div class="form-group col-12 col-sm-4">
            <label for="cad-cidade">Cidade</label>
            <input id="cad-cidade" class="form-control" type="text" name="register_cidade">
          </div>
          <div class="form-group col-12 col-sm-4">
            <label for="cad-bairro">Bairro</label>
            <input id="cad-bairro" class="form-control" type="text" name="register_bairro">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-12 col-sm-6">
            <label for="cad-email">E-mail</label>
            <input id="cad-email" class="form-control" type="text" name="register_email">
          </div>
          <div class="form-group col-12 col-sm-6">
            <label for="cad-confirmaemail">Confirmar e-mail</label>
            <input id="cad-confirmaemail" class="form-control" type="text" name="register_verifyemail">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-12 col-sm-6">
            <label for="cad-senha">Cadastre sua senha</label>
            <input id="cad-senha" class="form-control" type="password" name="register_password">
            <small>Sua senha deve conter de 6 a 10 caracteres.</small>
          </div>
          <div class="form-group col-12 col-sm-6">
            <label for="cad-senha">Confirme sua senha</label>
            <input id="cad-senha" class="form-control" type="password" name="register_password">
          </div>
        </div>
        <div class="form-row">
          <p>*Aceito receber novidades e informações da promoção e da Upfield via E-mail.</p>
        </div>
        <div class="form-row">
          <div class="form-check form-check-inline">
            <input id="user_optin_mail" class="form-check-input" type="radio" name="optradio" value="1">
            <label for="user_optin_mail" class="form-check-label radio">Sim</label>
          </div>
          <div class="form-check form-check-inline">
            <input id="user_optin_notmail" class="form-check-input" type="radio" name="optradio" value="0">
            <label for="user_optin_notmail" class="form-check-label radio">Não</label>
          </div>
        </div>
        <div class="form-row">
          <p>Para prosseguir, aceite os termos abaixo:</p>
        </div>
        <div class="form-row">
          <div class="form-check form-check-inline">
            <input id="cad-privacidade" class="form-check-input" type="checkbox" name="" value="true">
            <label for="cad-privacidade" class="form-check-label checkbox">Li e aceito os Termos de Política de
              Privacidade da
              promoção (<a href="regulamento.php">Regulamento</a>).</label>
          </div>
          <div class="form-check form-check-inline">
            <input id="cad-privacidade-upfield" class="form-check-input" type="checkbox" name="" value="true">
            <label for="cad-privacidade-upfield" class="form-check-label checkbox">Li e aceito os Termos de Política de
              Privacidade da Upfield.</label>
          </div>
          <div class="form-check form-check-inline">
            <input id="cad-privacidade-participacao" class="form-check-input" type="checkbox" name="" value="true">
            <label for="cad-privacidade-participacao" class="form-check-label checkbox">Ao preencher os dados para
              participação da presente promoção, concordo em fornecer estas <br>
              informações para a base de dados de campanhas exclusivas Upfield.</label>
          </div>
        </div>
        <br><br>
        <button type="submit" class="btn btn-lg align-self-center">CADASTRE SEUS DADOS</button>
      </form>
    </div>
  </div>
</section>

<!-- Footer -->
<?php include('./resources/views/parts/footer.php');?>